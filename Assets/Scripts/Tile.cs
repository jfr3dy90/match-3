using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Tile : MonoBehaviour
{
    public int xIndex;
    public int yIndex;

    Board m_board;

    internal void Init(int x, int y, Board board)
    {
        xIndex = x;
        yIndex = y;
        m_board = board;
    }

    private void OnMouseEnter()
    {
        m_board.DragToTile(this);
    }

    private void OnMouseDown()
    {
        m_board.ClickedTile(this);
    }

    private void OnMouseUp()
    {
        m_board.ReleaseTile();
    }
}
